const fetch = require('node-fetch');

const searchQ = async (req,res) =>{
   
    const query = req.query.q;
    console.log("searchQ", query);
    fetch( `https://api.mercadolibre.com/sites/MLA/search?q=:${query}`)
    .then(response => response.json())
    .then(data => {
        
        let elem = data.results.slice(0,4);
        for(let i=0 ; i < elem.length; i++){
            elem[i]={...elem[i] , author : {
                                          name: 'Mario',
                                          lastname: 'Brito'
                                          }
                   }
        }
        console.log(" Resultado 1:",elem[1]);
        res.status(200).json(elem);
    });  
}

const getById = async (req,res) =>{

  const id = req.params.id;
  console.log("getById", id);
  let descrip='';
  fetch(`https://api.mercadolibre.com/items/${id}/description`)
    .then(response => response.json())
    .then(data => {
        descrip=data.plain_text;
        fetch(`https://api.mercadolibre.com/items/${id}`)
            .then(response => response.json())
            .then(data => {
                console.log("RES",{...data, autor : { name: 'Mario' , lastname: 'Brito'}, 
                                            description: descrip
                                        });
                res.status(200).json({...data, autor : { name: 'Mario' , lastname: 'Brito'}, 
                                               description: descrip
                });
            }); 
    });
  
}

module.exports = {
    searchQ,
    getById
}