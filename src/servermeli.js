const express = require('express');
const cors = require("cors");

const app = express();

var corsOptions = {
    origin: "http://localhost:3000"
};
app.use(cors(corsOptions));
  

// midleware enteinde la funcion antes de que llegue la peticion
app.use( express.json());
app.use( express.urlencoded({ extended : false })); //extended para no recibir ninguna imagen

// rutas
app.use( require('./routes/index'));

app.listen(3001);
console.log('MELI Server on port 3001');