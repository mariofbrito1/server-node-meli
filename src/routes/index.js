const { Router } = require('express');
const router = Router();

const { getById, searchQ } = require('../controllers/index.controller') 


router.get('/api/items', searchQ );
//https://api.mercadolibre.com/sites/MLA/search?q=:query

router.get('/api/items/:id',getById),
//https://api.mercadolibre.com/items/:id
//https://api.mercadolibre.com/items/:id​/description 


module.exports = router;